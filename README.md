# Projet_na17_a19_groupe7

# Projet de conception et de réalisation de la base de données d'une place d'échange à l'UTC

Projet en cours de réalisation dans le cadre de l'UV Na17

 - Cahier des charges fourni :https://librecours.net/exercice/projet/projet.xhtml?part=19

# Membres du groupe 
    - RIGUIDEL Emma

    - SZERBOJM Julie

    - TAVERNIER Léo

    - ZHANG Jinjing

    - ZHENG Zilu
    
# Objet du projet

L'objectif du projet est de réaliser une place d'échange à l'UTC.

Le projet pourra intégrer des biens et des services.

Le projet pourra intégrer des actions de dons, d'échange ou de vente.

Le projet pourra intégrer la gestion d'une monnaie interne.

Les utilisateurs pourront déposer des offres ou des demandes, qui pourront être localisées dans le temps et l'espace.

Les annonces pourront être classées selon des rubriques, des mots-clés...

On peut ajouter des mécanismes d'enchère, de négociation...

Le projet pourra intégrer un système de communication interne.

Le projet pourra intégrer un historique des actions menées.

# Fichiers liés au projet
    - Note de clarification (NDC)
    
# Utilisateurs principaux
    - Administrateur du site
    - visiteur (consultation)
    - membre (achat vente ...)