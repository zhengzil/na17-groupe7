# MLD_Groupe7

```
Adresse(#idadresse : int, pays : string, codepost : int, rue : string, num: int, appart : int) avec pays, codepost, rue, num NOT NULL

Membre(#id :int, email : string, mdp : string, nom : string; prenom : string, tel : int, semestre : string, adresse=>Adresse, compte=>SoldeCompte) avec compte, email KEY

Administrateur(#login=>Membre, codePIN : int) avec codePIN NOT NULL

Groupe(#nom : string, creation : date, description : string, admin=>Administrateur) avec admin NOT NULL

SoldeCompte (#num : int, IBAN : string, solde : float) avec IBAN KEY

Virement (#idvir : int, montant : float, moment : date, libelle : string, crediteur=>SoldeCompte, debiteur=>SoldeCompte) avec crediteur, debiteur, montant, moment NOT NULL AND crediteur!=debiteur

MessagePrive (#idmess : int, moment : date, objet : string, texte : string, expediteur=>Membre, destinataire=>Membre) avec expediteur, destinataire, moment, texte NOT NULL AND destinataire!=expediteur

Annonce (#idann : int, titre : string, description : string, date_publie : date, statut : {en cours, terminée, annulée}, suppression : booleen, validation : booleen, type : {Troc, Emprunt, AchatVente}, echange : string, duree : time, prix: float, offert_par =>Membre, demande_par=>Membre, valide_par=>Administrateur, supprime_par=>admin, categorie=>Categorie) avec NOT ((type=Troc AND prix) OR (type=troc AND duree)), NOT (type=Emprunt AND echange), NOT ((type=AchatVente AND duree) OR (type=AchatVente AND echange)), (offert_par XOR demande_par), categorie NOT NULL AND (suppresion=True AND supprime_par) AND (validation=True AND valide_par)

Produit(#idprod : string, nom : string, etat : {neuf, bon etat, legerement abime, abime}, annonce=>Annonce) avec annonce NOT NULL

Categorie(#nom : {vetement, ustensile, scolaire, nourriture, mobilier, immobilier, evenement, 
autre}, description : string)

Photo (#url : string, hauteur : float, largeur : float, annonce=>Annonce) avec annonce NOT NULL

MotCle (#mot : string)

Communauté(#groupe=>Groupe, #membre=>Membre)

Partage(#annonce=>Annoce, #groupe=>Groupe)

AdresseLivraison (#adresse=>Adresse, #habitant=>Membre) avec adresse NOT NULL

Commentaire(#idcom : int, posteur=>Membre, annonce=>Annonce, date : date, contenu : string) avec date, contenu NOT NULL

Modification (#idmodif : int, editeur=>Administrateur, annonce=>Annonce, moment : date, modif : string) avec editeur, annonce, moment, modif NOT NULL

PossedeMotcCle (#motcle=>MotCle, #annonce=>Annonce)
```



<u>Contraintes complexes :</u>

* PROJECTION (Membre, compte) = PROJECTION (SoldeCompte, num)
* Entre 0 et 5 photos par Annonce



<u>Vues :</u>

vTroc = Restriction (Annonce, type = 'Troc')

vEmprunt = Restriction (Annonce, type = 'Emprunt')

vAchatVente = Restriction (Annonce, type = 'AchatVente')

vAdministrateur = JointureNaturelle (Membre, Administrateur)

vNonAdmin =Restriction( JointureExtreneGauche (Membre, Adminisatrateur), codePIN = NULL)



<u>Commentaires :</u>

* Nous avons choisi la clé artificielle comme clé primaire de la classe Membre. Le raison de ne pas choisir  l'attribut email avec des contraintes UNIQUE et NOT NULL, c'est que nous permettons à l'utilisateur de modifier l'e-mail, il ne s'agit pas d'une valeur fixe.

* Pour l'héritage entre Membre et Administrateur, nous choisissons la représentation par référence, car les administrateur ont plus de droits que les membres, mais les .
* Pour l'héritage entre la classe Annonce, Troc, Emprunt et AcahtVente, nous choisissons la représentation par la classe mère, c'est l'héritage presque complet et les classes fille n'ont pas d'association et ne possèdent pas les clés propres.





