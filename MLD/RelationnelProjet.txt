Adresse(#idadresse : string, pays : string, codepost : int, rue : string, num: int, appart : int) avec pays, codepost, rue, num NOT NULL

Membre(#login :string, email : string, mdp : string, nom : string; prenom : string, tel : int, semestre : string, adresse=>Adresse, compte=>SoldeCompte) avec email UNIQUE AND adresse NOT NULL AND compte KEY

Administrateur(#login=>Membre, codePIN : int) avec codePIN NOT NULL

Groupe(#nom : string, creation : date, description : string, admin=>Administrateur) avec admin NOT NULL

SoldeCompte (#num : int, IBAN : string, solde : float) avec IBAN UNIQUE

Virement (#idvir : int, montant : float, date : date, libelle : string, crediteur=>SoldeCompte, debiteur=>SoldeCompte) avec crediteur, debiteur NOT NULL AND crediteur!=debiteur

MessagePrive (#idmess : int, date : date, objet : string, texte : string, expediteur=>Membre, destinataire=>Membre) avec expediteur, destinataire NOT NULL AND destinataire!=expediteur

Annonce (#idann : int, titre : string, description : string, date_publie : date, statut : {en cours, terminée, annulée}, suppression : boolen, validation : booleen, type : {troc, emprunt, AchatVente}, echange : string, duree : time, prix: float, offert_par =>Membre, demande_par=>Membre, valide_par=>Administrateur, supprime_par=>admin, categorie=>Categorie) avec NOT ((type=troc AND prix) OR (type=troc AND duree)), NOT (type=Emprunt AND echange), NOT ((type=AchatVente AND duree) OR (type=AchatVente AND echange)), offert_par, demande_par, categorie NOT NULL AND NOT (demande_par AND offert_par), (suppresion=True AND supprime_par) AND (validation=True AND valide_par)

Produit(#idprod : string, nom : string, etat : {neuf, bon etat, legerement abime, abime}, correspond=>Annonce) avec correspond NOT NULL

Categorie(#nom : {vetement, ustensile, scolaire, nourriture, mobilier, immobilier, evenement, 
autre}, description : string)

Photo (#url : string, hauteur : float, largeur : float, annonce=>Annonce) avec annonce KEY

MotCle (#mot : string)

Communauté(#groupe=>Groupe, #membre=>Membre)

Partage(#annonce=>Annoce, #groupe=>Groupe)

AdresseLivraison (#adresse=>Adresse, #habitant=>Membre) avec adresse NOT NULL

Commentaire(#posteur=>Membre, #annonce=>Annonce, date : date, contenu : string) avec posteur, annonce NOT NULL

Modification (#admin=>Administrateur, #annonce=>Annonce, date : date, modif : string)

PossedeMotcCle (#motcle=>MotCle, #annonce=>Annonce)


CONTRAINTES COMPLEXES :
 - PROJECTION (Membre, compte) = PROJECTION (SoldeCompte, num)
 - Entre 0 et 5 photos par Annonce

