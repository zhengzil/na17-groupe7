-- adresse
INSERT INTO Adresse VALUES (1, 'France', '75011', 'Rue de Nice', 8, 13);
INSERT INTO Adresse VALUES (2, 'France', '60200', 'Rue du général lerlerc', 9, 3);
INSERT INTO Adresse VALUES (3, 'France', '60200', 'Rue Solférino', 35, 1);
INSERT INTO Adresse VALUES (4, 'France', '60200', 'Place du Change', 9, 1);
INSERT INTO Adresse VALUES (5, 'France', '75011', 'Rue de Nice', 8, 13);
INSERT INTO Adresse VALUES (6, 'France', '75011', 'Rue de Nice', 8, 12);
INSERT INTO Adresse VALUES (7, 'France', '75011', 'Rue de Nice', 8, 13);
INSERT INTO Adresse VALUES (8, 'France', '75011', 'Rue de Nice', 8, 12);

-- soldeCompte
INSERT INTO SoldeCompte VALUES (1, 'FR7612341234123412341234001', 5.00);
INSERT INTO SoldeCompte (num, IBAN) VALUES (2, 'FR7612341234123412341234002');
INSERT INTO SoldeCompte (num, IBAN) VALUES (3, 'FR7612341234123412341234003');
INSERT INTO SoldeCompte (num, IBAN) VALUES (4, 'FR7612341234123412341234004');
INSERT INTO SoldeCompte (num, IBAN) VALUES (5, 'FR7612341234123412341234005');

-- membre
INSERT INTO Membre VALUES (1, '1@1.com', '111', 'zhang', 'jinjing', '0663892001', '2019A', 1, 1);
INSERT INTO Membre VALUES (2, '2@2.com', '222', 'szerbojm', 'julie', '0663892002', '2019A', 5, 2);
INSERT INTO Membre VALUES (3, '3@3.com', '333', 'tavernier', 'léo', '0663892003', '2019A', 6, 3);
INSERT INTO Membre VALUES (4, '4@4.com', '444', 'riguidel', 'emma', '0663892004', '2019A', 7, 4);
INSERT INTO Membre VALUES (5, '5@5.com', '555', 'zheng', 'zilu', '0663892005', '2019A', 8, 5);
INSERT INTO Membre (id, email, mdp, nom, prenom) VALUES (6, '6@6.com', '666', 'admin', 'admin');

-- administrateur
INSERT INTO Administrateur VALUES (6, '123456');

-- groupe
INSERT INTO Groupe (nom, creation, description, admin)VALUES ('etudiants', to_timestamp('01/11/2019 11:11:11', 'DD/MM/YYYY HH24:MI:SS'), 'cest une groupe pour les étudiants', 6);

-- messagePrive
INSERT INTO MessagePrive VALUES (1, 1, 2, to_timestamp('01/11/2019 11:13:11', 'DD/MM/YYYY HH24:MI:SS'), 'enchatée', 'hello');
INSERT INTO MessagePrive VALUES (2, 2, 1, to_timestamp('01/11/2019 11:14:11', 'DD/MM/YYYY HH24:MI:SS'), 'enchatée', 'hello');

-- categorie
INSERT INTO Categorie(nom) VALUES ('vêtement');
INSERT INTO Categorie(nom) VALUES ('ustensile');
INSERT INTO Categorie(nom) VALUES ('scolaire');
INSERT INTO Categorie(nom) VALUES ('nourriture');
INSERT INTO Categorie(nom) VALUES ('mobilier');
INSERT INTO Categorie(nom) VALUES ('immobilier');
INSERT INTO Categorie(nom) VALUES ('évènement');
INSERT INTO Categorie(nom) VALUES ('autres');

-- Annonce
INSERT INTO Annonce(idann, titre, description, date_publi, statut, type, prix, offert_par, valide_par, validation, categorie) VALUES (1, 'a good object', 'its a good object', to_timestamp('31/10/2019 00:00:00', 'DD/MM/YYYY HH24:MI:SS'), 'terminée', 'AchatVente', 5.00, 2, 6, TRUE, 'autres');

-- virement
--INSERT INTO Virement Values (1, 1, 2, 5.00, to_timestamp('02/11/2019 22:22:22', 'DD/MM/YYYY HH24:MI:SS') , 1, 'workbook');
