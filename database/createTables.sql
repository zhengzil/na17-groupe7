DROP TABLE IF EXISTS PossedeMotcCle;
DROP TABLE IF EXISTS Modification;
DROP TABLE IF EXISTS Commentaire;
DROP TABLE IF EXISTS Partage;
DROP TABLE IF EXISTS Communaute;
DROP TABLE IF EXISTS MotCle;
DROP TABLE IF EXISTS AdresseLivraison;
DROP TABLE IF EXISTS Photo;
DROP TABLE IF EXISTS Produit;
DROP TABLE IF EXISTS Annonce;
DROP TABLE IF EXISTS MessagePrive;
DROP TABLE IF EXISTS Categorie;
DROP TABLE IF EXISTS Virement;
DROP TABLE IF EXISTS Groupe;
DROP TABLE IF EXISTS Administrateur, Membre;
DROP TABLE IF EXISTS SoldeCompte;
DROP TABLE IF EXISTS Adresse;

CREATE TABLE Adresse (
    idadresse INTEGER,
    pays VARCHAR(50) NOT NULL,
    code_postal CHAR(5) NOT NULL,
    rue VARCHAR(255) NOT NULL,
    num INTEGER NOT NULL,
    appart INTEGER,
    PRIMARY KEY (idadresse)
);

CREATE TABLE SoldeCompte (
    num INTEGER,
    IBAN CHAR(27) NOT NULL UNIQUE,
    solde DECIMAL(9,2) DEFAULT 0.00,
    PRIMARY KEY (num)
);

CREATE TABLE Membre (
    id INTEGER,
    email VARCHAR(100) NOT NULL UNIQUE,
    mdp VARCHAR(255) NOT NULL,
    nom VARCHAR(50) NOT NULL,
    prenom VARCHAR(50) NOT NULL,
    tel VARCHAR(20) UNIQUE,
    semestre CHAR(5),
    adresse INTEGER,
    compte INTEGER UNIQUE,
    PRIMARY KEY (id),
    FOREIGN KEY (adresse) REFERENCES Adresse(idadresse),
    FOREIGN KEY (compte) REFERENCES SoldeCompte(num)
);

CREATE TABLE Administrateur (
    id INTEGER,
    codePIN INTEGER NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id) REFERENCES Membre(id)
);
ALTER TABLE Membre
ADD bannisseur INTEGER REFERENCES Administrateur(id);
ALTER TABLE Membre
ADD CHECK (id != bannisseur);

CREATE TABLE Groupe (
    nom VARCHAR(255),
    creation TIMESTAMP,
    description TEXT,
    admin INTEGER NOT NULL,
    PRIMARY KEY (nom),
    FOREIGN KEY (admin) REFERENCES Administrateur(id)
);

CREATE TABLE MessagePrive (
    idmess INTEGER,
    expediteur INTEGER NOT NULL,
    destinataire INTEGER NOT NULL,
    moment TIMESTAMP NOT NULL,
    objet VARCHAR(255),
    texte TEXT NOT NULL,
    PRIMARY KEY (idmess),
    FOREIGN KEY (expediteur) REFERENCES Membre(id),
    FOREIGN KEY (destinataire) REFERENCES Membre(id),
    CHECK (expediteur != destinataire)
);

CREATE TABLE Categorie (
    nom VARCHAR(20),
    description TEXT,
    PRIMARY KEY (nom),
    CHECK (nom IN ('vêtement', 'ustensile', 'scolaire', 'nourriture', 'mobilier', 'immobilier', 'évènement', 'autres'))
);

CREATE TABLE Annonce (
    idann INTEGER,
    titre VARCHAR(255),
    description TEXT NOT NULL,
    date_publi TIMESTAMP NOT NULL,
    statut VARCHAR(10),
    type VARCHAR(10),
    echange VARCHAR(255),
    duree INTERVAL,
    prix DECIMAL(9,2),
    offert_par INTEGER,
    demande_par INTEGER,
    valide_par INTEGER,
    validation BOOLEAN,
    supprime_par INTEGER,
    suppression BOOLEAN,
    categorie VARCHAR(20),
    PRIMARY KEY (idann),
    FOREIGN KEY (categorie) REFERENCES Categorie(nom),
    FOREIGN KEY (offert_par) REFERENCES Membre(id),
    FOREIGN KEY (demande_par) REFERENCES Membre(id),
    FOREIGN KEY (supprime_par) REFERENCES Administrateur(id),
    FOREIGN KEY (valide_par) REFERENCES Administrateur(id),
    CHECK (statut IN ('en cours', 'terminée', 'annulée')),
    CHECK (type IN ('Troc', 'Emprunt', 'AchatVente')),
    CHECK (NOT (type = 'Troc' AND (prix IS NOT NULL OR duree IS NOT NULL))),
    CHECK (NOT (type = 'Emprunt' AND echange IS NOT NULL)),
    CHECK (NOT (type = 'AchatVente' AND (echange IS NOT NULL OR duree IS NOT NULL))),
    CHECK ((offert_par IS NOT NULL AND demande_par IS NULL) OR (demande_par IS NOT NULL AND offert_par IS NULL)),
    CHECK (NOT (validation = TRUE AND valide_par IS NULL)),
    CHECK (NOT (suppression = FALSE AND supprime_par IS NULL))
);
-- test check 
-- INSERT INTO Categorie (nom, description) VALUES ('autres', 'for UTCeens');
-- INSERT INTO Annonce (idannonce, titre, createur, description, date_publi, statut, categorie, type) VALUES (1, 'I want to exchange a watch', 'qqq', 'My watch is very nice ! I can accept any exchange !', '2019-10-14 21:51:00', 'en cours', 'autres', 'Troc');
-- UPDATE Annonce SET prix = 0.01 WHERE idannonce = 1;

CREATE TABLE Produit (
    idprod INTEGER,
    nom VARCHAR(50),
    etat VARCHAR(20),
    annonce INTEGER NOT NULL,
    CHECK (etat IN ('neuf', 'bon état', 'légèrement abîmé', 'abîmé')),
    PRIMARY KEY (idprod),
    FOREIGN KEY (annonce) REFERENCES Annonce(idann)
);
/*
Contrainte complexe : 
Projection(Annonce, idannonce) = Projection(Produit, annonce)
*/

CREATE TABLE Virement (
    idvir INTEGER,
    crediteur INTEGER NOT NULL,
    debiteur INTEGER NOT NULL,
    montant DECIMAL(9,2) NOT NULL,
    moment TIMESTAMP NOT NULL,
    produit INTEGER NOT NULL,
    libelle TEXT,
    PRIMARY KEY (idvir),
    FOREIGN KEY (crediteur) REFERENCES Membre(id),
    FOREIGN KEY (debiteur) REFERENCES Membre(id),
    FOREIGN KEY (produit) REFERENCES Produit(idprod),
    CHECK (crediteur != debiteur)
);

CREATE TABLE Photo (
    url BYTEA,
    hauteur FLOAT,
    largeur FLOAT,
    annonce INTEGER NOT NULL,
    PRIMARY KEY (url),
    FOREIGN KEY (annonce) REFERENCES Annonce(idann)
);
/*
Contrainte complexe : 
Une annonce possède au plus de 5 photos
*/

CREATE TABLE AdresseLivraison (
    habitant INTEGER NOT NULL,
    adresse INTEGER NOT NULL,
    PRIMARY KEY (habitant, adresse),
    FOREIGN KEY (habitant) REFERENCES Membre(id),
    FOREIGN KEY (adresse) REFERENCES Adresse(idadresse)
);

CREATE TABLE MotCle (
    nom VARCHAR(20),
    PRIMARY KEY (nom)
);

CREATE TABLE Communaute (
    groupe VARCHAR(255),
    membre INTEGER,
    PRIMARY KEY (groupe, membre),
    FOREIGN KEY (groupe) REFERENCES Groupe(nom),
    FOREIGN KEY (membre) REFERENCES Membre(id)
);

CREATE TABLE Partage (
    groupe VARCHAR(255),
    annonce INTEGER,
    PRIMARY KEY (groupe, annonce),
    FOREIGN KEY (groupe) REFERENCES Groupe(nom),
    FOREIGN KEY (annonce) REFERENCES Annonce(idann)
);

CREATE TABLE Commentaire (
    idcom INTEGER,
    posteur INTEGER NOT NULL,
    annonce INTEGER NOT NULL,
    date TIMESTAMP NOT NULL,
    contenu TEXT NOT NULL,
    PRIMARY KEY (idcom),
    FOREIGN KEY (posteur) REFERENCES Membre(id),
    FOREIGN KEY (annonce) REFERENCES Annonce(idann)
);

CREATE TABLE Modification (
    idmodif INTEGER,
    annonce INTEGER NOT NULL,
    editeur INTEGER NOT NULL,
    moment TIMESTAMP NOT NULL,
    motif TEXT NOT NULL,
    PRIMARY KEY (idmodif),
    FOREIGN KEY (annonce) REFERENCES Annonce(idann),
    FOREIGN KEY (editeur) REFERENCES Administrateur(id)
);

CREATE TABLE PossedeMotcCle (
    annonce INTEGER,
    motcle VARCHAR(20),
    PRIMARY KEY (annonce, motcle),
    FOREIGN KEY (annonce) REFERENCES Annonce(idann),
    FOREIGN KEY (motcle) REFERENCES MotCle(nom)
);

CREATE VIEW AnnonceTrocExisteEncours AS 
    SELECT idann, titre, description, date_publi, echange, offert_par, demande_par, valide_par, validation, categorie
    FROM Annonce
    WHERE statut = 'en cours'
    AND suppression = FALSE
    AND type = 'Troc';

CREATE VIEW AnnonceEmpruntExisteEncours AS 
    SELECT idann, titre, description, date_publi, prix, duree, offert_par, demande_par, valide_par, validation, categorie
    FROM Annonce
    WHERE statut = 'en cours'
    AND suppression = FALSE
    AND type = 'Emprunt';

CREATE VIEW AnnonceAchatVenteExisteEncours AS 
    SELECT idann, titre, description, date_publi, prix, offert_par, demande_par, valide_par, validation, categorie
    FROM Annonce
    WHERE statut = 'en cours'
    AND suppression = FALSE
    AND type = 'AchatVente';