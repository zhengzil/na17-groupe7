#!/usr/bin/python3
import connect2 as conn

def showAdresse():
    # connect to the db
    cur = conn._connect()
    sql = "SELECT * FROM Adresse"
    cur.execute(sql)
    # Fetch data line by line
    raw = cur.fetchone()
    print("Adresse : ")
    print("--------------------")
    while raw:
        print("Adresse ID : ",raw[0])
        print("Pays : ", raw[1])
        print("code_postal : ",raw[2])
        print("rue : ", raw[3])
        print("num : ", raw[4])
        print("appart : ", raw[5])
        print("--------------------")
        raw = cur.fetchone()
    conn._close()
#showAdresse()
def showSoldeCompte():
    cur = conn._connect()
    sql = "SELECT * FROM SoldeCompte"
    cur.execute(sql)
    # Fetch data line by line
    raw = cur.fetchone()
    print("SoldeCompte : ")
    print("--------------------")
    while raw:
        print("num : ", raw[0])
        print("IBAN : ", raw[1])
        print("solde : ", raw[2])
        print("--------------------")
        raw = cur.fetchone()
    conn._close()
#showSoldeCompte()
def showMembre():
    cur = conn._connect()
    sql = "SELECT * FROM Membre"
    cur.execute(sql)
    # Fetch data line by line
    raw = cur.fetchone()
    print("Membre : ")
    print("--------------------")
    while raw:
        print("id : ", raw[0])
        print("email : ", raw[1])
        print("mdp : ", raw[2])
        print("nom : ", raw[3])
        print("prenom : ", raw[4])
        print("tel : ", raw[5])
        print("semestre : ", raw[6])
        print("tel : ", raw[7])
        print("adresse : ", raw[8])
        print("compte : ", raw[5])
        print("--------------------")
        raw = cur.fetchone()
    conn._close()
#showMembre()
def showCategorie():
    cur = conn._connect()
    sql = "SELECT * FROM Categorie"
    cur.execute(sql)
    # Fetch data line by line
    raw = cur.fetchone()
    print("Categorie : ")
    print("--------------------")
    while raw:
        print("nom : ", raw[0])
        print("--------------------")
        raw = cur.fetchone()
    conn._close()
#showCategorie()
def showTableAsYouWish(table=''):
    cur = conn._connect()
    if table == '':
        table = input("Quelle table voulez vous voir? (Table par defaut Adresse)")
        if table == '':
            table = "Adresse"
    sql = "SELECT * FROM " + table
    cur.execute(sql)
    # Fetch data line by line
    raw = cur.fetchone()
    print(table, " : ")
    print("--------------------")
    while raw:
        for i in raw:
            print(i)
        print("--------------------")
        raw = cur.fetchone()
    conn._close()
#showTableAsYouWish('Membre')
def queryAsYouWish(requete=''):
    cur = conn._connect()
    if requete=='':
        requete =  input("Entrez une SQL requete (requet par defaut SELECT * FROM Adresse) : ")
        if requete == '':
            requete = "SELECT * FROM Adresse"
    cur.execute(requete)
    # Fetch data line by line
    raw = cur.fetchone()
    print(requete, " : ")
    print("--------------------")
    while raw:
        for i in raw:
            print(i)
        print("--------------------")
        raw = cur.fetchone()
    conn._close()
#queryAsYouWish('SELECT * FROM Membre')