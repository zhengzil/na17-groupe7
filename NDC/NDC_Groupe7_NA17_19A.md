# UTroC

### Note de clarification du projet 19A de NA17

L'objectif de notre projet est de réaliser une base de données permettant de gérer les échanges, emprunts, achats et ventes ayant eu lieu entre UTCéens. La création de la base de données qui alimentera le site internet de la place d'échange se fera par la suite en utilisant le langage HTML/CSS et PHP.

Nous allons lister les objets, les attributs, les fonctions liées à eux, etc. nécessaires pour construire la base de données dans les sections suivantes.

#### Liste des objets qui devront être gérés dans la base de données

- Membre
- Administrateur
- Groupe
- Message Privé
- Adresse 
- Compte
- Virement
- Produit
- Offre
- Demande
- Commentaire
- Catégorie
- Photo
- Mot clé



#### Liste des propriétés associées à chaque objet

Il existe différents types d’utilisateurs :

- **Membre**
  - login : string 
  - email : string 
  - mdp : string 
  - nom : string 
  - prénom : string 
  - téléphone : integer
  - semestre : string

- **Administrateur**

  - codePIN : int

    Le code pin est demandé à l’utilisateur lorsque celui-ci veut supprimer des annonces ou des messages, les modifier, gérer un groupe...

Un membre est relié à une adresse dans laquelle il réside (adresse principale), et à 1 ou plusieurs adresses de livraison (un étudiant peut être à l’étranger pendant un semestre).
- **Adresse**
  - pays : string
  - codepostal : integer
  - rue : string
  - num : integer
  - appart : integer (si l'utilisateur ne vit pas dans une maison)


Un membre peut effectuer plusieurs actions :

\-       proposer un produit à vendre

\-       proposer un produit à l’échange

\-       proposer un produit à l’emprunt

\-       rechercher un produit à acheter / échanger / emprunter

\-       poster une demande de produit à acheter / échanger / emprunter

\-       publier un commentaire sur une annonce

\-       envoyer un message privé à un ou plusieurs membres

\-       s'il est administrateur, modifier, valider ou supprimer une annonce

\-       appartenir à un groupe




Chaque produit est identifié grâce à une clé artificielle :

- **Produit**
  - id  : string 
  - nom : string 
  - etat : { neuf | bon état | légèrement abimé | abîmé}




Une annonce (offre OU demande) est postée par un seul membre, mais un membre peut poster plusieurs annonces.

- **Annonce**
  - titre : string
  - description : string
  - date_publi : Date
  - suppression : booléen
  - validation : booléen 
  - statut : { en cours | terminée | annulée }
  - type : {troc | emprunt | achat/vente}
  - prix : float (seulement si emprunt ou achat/vente)
  - duree : integer (seulement si emprunt)
  - echange : string (seulement si troc)
 

Une annonce appartient à une catégorie, peut posséder des mots-clés qui permettent de la retrouver, et peut être représentée par 0 à 5 images.

- **Catégorie**
  - nom : {vêtement | ustensile | scolaire | nourriture | mobilier | immobilier | évènement | autre}
  - description : string
- **Photo**
  - url : string
  - largeur : float
  - hauteur : float

- **Mot clé**
  - mot : string
  

Chaque membre dispose d'un compte :

- **SoldeCompte**
  - num : integer
  - IBAN : string
  - solde : integer
  

Le solde du compte varie lorsqu'un virement est effectué, c'est-à-dire lorsque le membre vend ou achète un produit : 

- **Virement**
  - montant : float
  - date : date
  - libellé : string

Un membre peut contacter un autre membre, par exemple afin d’obtenir des informations sur une annonce, ou de négocier le prix du produit :

- **MessagePrive**
  - objet : string
  - date : datetime
  - texte : string
  

Un membre peut, s'il est adminstrateur, modifier( éventuellement plusieurs fois), valider ou supprimer une annonce :

- **Modification**
  - date : string
  - modif : string
  

Tout membre peut également appartenir à un ou plusieurs groupes en fonction de ses intérêts

- **Groupe**
  - nom : string
  - creation : date
  - description : string 
  

    

#### Liste des contraintes associées à ces objets et propriétés

- Un utilisateur peut publier plusieurs annonces, une annonce est publiée par un seul utiisateur.
- Un utilisateur a une adresse principale et 1 ou plusieurs adresses de livraison, une adresse peut correspondre à plusieurs étudiants (colocation)
- Un utilisateur dispose d'1 seul compte, et 1 compte n'appartient qu'à 1 utilisateur.
- Un compte peut être crédité de plusieurs virements, en revanche, un virement débite un seul compte et en crédite un seul
- Un utilisateur peut appartenir à plusieurs groupes, et un groupe contient plusieurs utilisateurs
- Tous les administrateurs sont des utilisateurs mais seuls quelques utilisateurs sont aussi adminsitrateurs.
- Un groupe est administré par un administrateur, et un administrateur peut administrer plusieurs groupes.
- Un groupe peut partager des annonces
- Un membre peut envoyer et recevoir plusieurs messages privés, mais un message n'est envoyé et reçu que par un membre
- Un administrateur peut modifier plusieurs annonces et une annonce peut être modifié par plusieurs administrateurs
- Un administrateur peut valider ou supprimer plusieurs annonces et une annonce peut être validée ou supprimée par seulement un administrateur
- Une annonce est une offre ou une demande, elle est forcément l'une des deux mais ne peut pas être les 2 en même temps.
- Une annonce est soit de type emprunt, soit de type Achat/vente ou soit de type troc.
- Une annonce concerne un ou plusieurs produit(s), mais un produit n'est référencé que dans une annonce (par exemple, on considère qu'un produit n'est pas un modèle stylo, mais un dit stylo unique de ce modèle)
- Un produit peut avoir une catégorie (une catégorie référence plusieurs produits), plusieurs mots clés et au plus 5 images
- Un utilisateur peut commenter une annonce, et un commentaire est écrit par un seul utilisateur


#### Liste des utilisateurs (rôles) appelés à modifier et consulter les données

- Membres
- Administrateurs

#### Liste des fonctions que ces utilisateurs pourront effectuer

- Membres :
  - consulter les annonces
  - poster des annonces (vente, echange, emprunt)
  - répondre à des annonces (achat, echange, emprunt)
  - ecrire des commentaires sur une annonce
  - Envoyer (ou recevoir) des messages privés
  - appartenir à un groupe
- Administrateurs :
  - consulter les annonces
  - poster des annonces (vente, echange, emprunt)
  - répondre à des annonces (achat, echange, emprunt)
  - ecrire des commentaires sur une annonce
  - Envoyer (ou recevoir) des messages privés
  - appartenir à un groupe 
  - administrer un groupe
  - valider les annonces
  - supprimer les annonces
  - ecrire des commentaires
  - modifier les annonces