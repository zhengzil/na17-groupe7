DROP TABLE Membre;
DROP TABLE Annonce;

CREATE OR REPLACE TYPE typAdresse AS OBJECT (
    pays VARCHAR2(50),
    code_postal CHAR(5),
    rue VARCHAR2(255),
    num NUMBER(3),
    appart NUMBER(3));
/
CREATE TABLE Adresse OF typAdresse (
    PRIMARY KEY (pays, code_postal, rue, num, appart)
);

CREATE OR REPLACE TYPE typSoldeCompte AS OBJECT (
    IBAN CHAR(27),
    solde NUMBER(9,2));
/
CREATE TABLE SoldeCompte OF typSoldeCompte (
    PRIMARY KEY (IBAN)
);

CREATE OR REPLACE TYPE typMembre AS OBJECT (
    email VARCHAR2(100),
    mdp VARCHAR2(255),
    nom VARCHAR2(50),
    prenom VARCHAR2(50),
    tel VARCHAR2(20),
    semestre CHAR(5),
    adresse REF typAdresse,
    compte REF typSoldeCompte);
/

CREATE TABLE Membre OF typMembre (
    PRIMARY KEY (email),
    adresse NOT NULL,
    compte NOT NULL,
    SCOPE FOR (adresse) IS Adresse,
    SCOPE FOR (compte) IS SoldeCompte
);

CREATE TABLE adresseLivraison (
    membre REF typMembre,
    adresse REF typAdresse,
    SCOPE FOR (membre) IS Membre,
    SCOPE FOR (adresse) IS Adresse
);

CREATE OR REPLACE TYPE typMotCles AS TABLE OF VARCHAR2(50);
/
CREATE OR REPLACE TYPE typPhoto AS OBJECT (
    url BLOB,
    hauteur NUMBER(3,2),
    largeur NUMBER(3,2))
/
CREATE OR REPLACE TYPE typListePhoto AS TABLE OF typPhoto;
/

CREATE OR REPLACE TYPE typAnnonce AS OBJECT (
    idann NUMBER(5),
    titre VARCHAR2(255),
    description VARCHAR2(1000),
    date_publi DATE,
    statut VARCHAR2(10),
    type VARCHAR2(10),
    echange VARCHAR2(255),
    duree INTERVAL DAY TO SECOND,
    prix NUMBER(9,2),
    offert_par REF typMembre,
    demande_par REF typMembre,
    motCles typMotCles,
    photos typListePhoto
);
/

CREATE TABLE Annonce OF typAnnonce (
    PRIMARY KEY (idann),
    SCOPE FOR (offert_par) IS Membre,
    SCOPE FOR (demande_par) IS Membre,
    CHECK (statut IN ('en cours', 'terminée', 'annulée')),
    CHECK (type IN ('Troc', 'Emprunt', 'AchatVente')),
    CHECK (NOT (type = 'Troc' AND (prix IS NOT NULL OR duree IS NOT NULL))),
    CHECK (NOT (type = 'Emprunt' AND echange IS NOT NULL)),
    CHECK (NOT (type = 'AchatVente' AND (echange IS NOT NULL OR duree IS NOT NULL))),
    CHECK ((offert_par IS NOT NULL AND demande_par IS NULL) OR (demande_par IS NOT NULL AND offert_par IS NULL)))
NESTED TABLE motCles STORE AS annonce_nt1
NESTED TABLE photos STORE AS annonce_nt2;

CREATE OR REPLACE TYPE typProduit AS OBJECT (
    nom VARCHAR2(50),
    etat VARCHAR2(20),
    annonce REF typAnnonce)
/
CREATE TABLE Produit (
    PRIMARY KEY (nom, annonce),
    annonce NOT NULL,
    SCOPE FOR (annonce) IS Annonce
);

CREATE TABLE Commentaire (
    annonce REF typAnnonce,
    membre REF typMembre,
    SCOPE FOR (membre) IS Membre,
    SCOPE FOR (annonce) IS Annonce,
    contenu VARCHAR2(255),
    date DATE
);