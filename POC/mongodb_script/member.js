conn = new Mongo();
db = conn.getDB("na17");
db.Member.drop();
db.Member.insert({
    email: "1@1.com",
    mdp: "111",
    nom: "zhang",
    prenom: "jinjing",
    tel: "0663892001",
    semestre: "2019A",
    adressePrincipale: {
        pays: "France",
        codepost: "75011",
        rue: "Rue de Nice",
        num: 8,
        appart: 13
    },
    soldeCompte: {
        IBAN: "FR7612341234123412341234001",
        solde: 5.00
    },
    adresseLivraiason: [{
        pays: "France",
        codepost: "60200",
        rue: "Rue du général lerlerc",
        num: 9,
        appart: 3
    }, {
        pays: "France",
        codepost: "60200",
        rue: "Rue Solférino",
        num: 35
    }, {
        pays: "France",
        codepost: "75011",
        rue: "Rue de Nice",
        num: 8,
        appart: 13
    }]
})
db.Member.insert({
    email: "2@2.com",
    mdp: "222",
    nom: "szerbojm",
    prenom: "julie",
    tel: "0663892002",
    semestre: "2019A",
    adressePrincipale: {
        pays: "France",
        codepost: "75011",
        rue: "Rue de Nice",
        num: 8,
        appart: 13
    },
    soldeCompte: {
        IBAN: "FR7612341234123412341234002",
        solde: 10.00
    }
})