conn = new Mongo();
db = conn.getDB("na17");

db.Annonce.drop();

var date_publi = new Date("2019-10-31T00:00:00Z");
var date1 = new Date("2019-11-01T11:11:11Z")
var date2 = new Date("2019-11-02T22:22:22Z")
db.Annonce.insert({
    titre: "good objects",
    description: "there're many good objects",
    date_publi: date_publi,
    statut: "terminée",
    type: "AchatVente",
    prix: 5.00,
    offert_par: ObjectId("5de537d0561f9092c82000c9"),
    categorie: {
        nom: 'autres',
        description: 'description for the other categories'
    },
    motsCle: ['good', 'object', 'étudiant'],
    produit: [{
        nom: "abc",
        etat: "neuf"
    }, {
        nom: "bcd",
        etat: "bon etat"
    }],
    photos: [{
        url: "ww",
        hauteur: 10.00,
        largeur: 10.00
    }, {
        url: "www"
    }],
    commentaires: [{
        date: date1,
        contenu: "I like it",
        expediteur: ObjectId("5de537d0561f9092c82000c8")
    }, {
        date: date2,
        contenu: "I like it very much",
        expediteur: ObjectId("5de537d0561f9092c82000c8")
    }]
})