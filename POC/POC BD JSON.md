# POC BD JSON

Ci-dessous, c'est un UML simple représentant les relations entre eux.

<img src="./bdjson_uml.png" alt="bdjson_uml" style="zoom:80%;" />

[Cliquer ici pour voir le script de créer la base de données](https://gitlab.utc.fr/zhengzil/na17-groupe7/tree/master/POC/bdjson_script)

