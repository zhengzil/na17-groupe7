# POC RO v1

Nous voulons représenter une partie en RO. 

- Toutes les clés artificielles dans les relations imbriquées sont supprimées.
- Une annonce contient au plus de 5 images et un ou plusieurs produits. C'est plus pratique de présenter les compositions en MongoDB. En même temps, c'est plus facile de contrôler le nombre de photos pour une annonce.d

- Ici, nous ne pouvons pas imbriquer la relations Commentaire comme en MongoDB, c'est parce que nous ne pouvons pas référencer une clé étrangère dans la table imbriquée. Pour savoir le posteur de commentaire, nous gradons la struture d'association.
- L'association Commentaire entre Membre et Annonce est modifiée. Nous croyons que les commentaires doit être les compositions de la relation Annonce, car ils ne sont que demandés avec les annonces. En même temps, pour savoir ce qui crée ce commentaire, nous référence la clé primaire du membre dans le commentaire.
- La relation SoldeCompte est intégrée directement dans Membre, car tout le monde n'a qu'un compte, est les virements va relier la clé primaire du membre.

Ci-dessous, c'est un UML simple représentant les relations entre eux.

<img src="./rov1_uml.png" alt="rov1_uml" style="zoom:80%;" />

[Cliquer ici pour voir le script de créer la base de données de RO](https://gitlab.utc.fr/zhengzil/na17-groupe7/blob/master/POC/rov1.sql)