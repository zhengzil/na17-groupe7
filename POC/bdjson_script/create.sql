DROP TABLE IF EXISTS Membre;
DROP TABLE IF EXISTS Annonce;
CREATE TABLE Membre (
    id INTEGER,
    email VARCHAR(100) NOT NULL UNIQUE,
    mdp VARCHAR(255) NOT NULL,
    nom VARCHAR(50) NOT NULL,
    prenom VARCHAR(50) NOT NULL,
    tel VARCHAR(20) UNIQUE,
    adressePrincipale JSON,
    soldeCompte JSON,
    adresseLivraiason JSON,
    PRIMARY KEY (id)
);

CREATE TABLE Categorie (
    nom VARCHAR(20),
    description TEXT,
    PRIMARY KEY (nom),
    CHECK (nom IN ('vêtement', 'ustensile', 'scolaire', 'nourriture', 'mobilier', 'immobilier', 'évènement', 'autres'))
);

CREATE TABLE Annonce (
    idann INTEGER,
    titre VARCHAR(255),
    description TEXT NOT NULL,
    date_publi TIMESTAMP NOT NULL,
    statut VARCHAR(10),
    offert_par INTEGER,
    demande_par INTEGER,
    categorie VARCHAR(20),
    motsCle JSON,
    produits JSON,
    photos JSON,
    commentaires JSON,
    PRIMARY KEY (idann),
    FOREIGN KEY (categorie) REFERENCES Categorie(nom),
    FOREIGN KEY (offert_par) REFERENCES Membre(id),
    FOREIGN KEY (demande_par) REFERENCES Membre(id),
    CHECK (statut IN ('en cours', 'terminée', 'annulée')),
    CHECK ((offert_par IS NOT NULL AND demande_par IS NULL) OR (demande_par IS NOT NULL AND offert_par IS NULL))
);