INSERT INTO Categorie VALUES (
    'autres',
    'description for the other categories'
);

INSERT INTO Membre VALUES (
    1,
    '1@1.com',
    '111',
    'zhang',
    'jinjing',
    '0663892001',
    '{
        "pays": "France", 
        "codepost": "75011", 
        "rue": "Rue de Nice", 
        "num": 8, 
        "appart": 13
    }',
    '{
        IBAN: "FR7612341234123412341234001",
        solde: 5.00
    }',
    '[
        {
            "pays": "France", 
            "codepost": "60200", 
            "rue": "Rue du général lerlerc", 
            "num": 9, 
            "appart": 3
        }, {
            "pays": "France", 
            "codepost": "60200",
            "rue": "Rue Solférino",
            "num": 35
        }, {
            "pays": "France",
            "codepost": "75011",
            "rue": "Rue de Nice",
            "num": 8,
            "appart": 13
        }
    ]'

);

INSERT INTO Membre (id, email, mdp, nom, prenom, tel, adressePrincipale, soldeCompte) VALUES (
    2,
    '2@2.com',
    '222',
    'szerbojm',
    'julie',
    '0663892002',
    '{
        "pays": "France",
        "codepost": "75011",
        "rue": "Rue de Nice",
        "num": 8,
        "appart": 13
    }',
    '{
        "IBAN": "FR7612341234123412341234002",
        "solde": 10.00
    }'
);

INSERT INTO Annonce(idann, titre, description, date_publi, statut, offert_par, categorie, motsCle, produit, photos) VALUES (
    1,
    'good objects',
    'there''re many good objects',
    TO_DATE('01/11/2019 11:13:11'),
    'terminée',
    1,
    1,
    '["good", "object", "étudiant"]',
    '[{
        "nom": "abc",
        "etat": "neuf"
    }, {
        "nom": "bcd",
        "etat": "bon etat"
    }]',
    '[{
        "url": "ww",
        "hauteur": 10.00,
        "largeur": 10.00
    }, {
        "url": "www"
    }]'
);