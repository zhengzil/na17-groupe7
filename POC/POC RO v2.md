# POC RO v2

Nous voulons représenter une partie en RO avec objectID. 

- Toutes les clés artificielles sont supprimés car nous pouvons utiliser l'objectID.
- Une annonce contient au plus de 5 images et un ou plusieurs produits. C'est plus pratique de présenter les compositions en MongoDB. En même temps, c'est plus facile de contrôler le nombre de photos pour une annonce. Donc nous gardons les tables impriqués dans la relation Annonce.

Ci-dessous, c'est un UML simple représentant les relations entre eux.

<img src="./rov2_uml.png" alt="rov1_uml" style="zoom:80%;" />

[Cliquer ici pour voir le script de créer la base de données de ROv2](https://gitlab.utc.fr/zhengzil/na17-groupe7/blob/master/POC/rov2.sql)