# POC MongoDB

Nous voulons représenter une partie en MongoDB. 

- Toutes les clés artificielles sont supprimées car l'insertion associe un identifiant (*_id*) à chaque document de la collection.
- Une annonce contient au plus 5 images et un ou plusieurs produits. C'est plus pratique de présenter les compositions en MongoDB. En même temps, c'est plus facile de contrôler le nombre de photos pour une annonce.d

- Les classes Catégorie et MotCle sont enregistrées directement dans la relation Annonce en tant qu'attributs. C'est plus pratique de trouver la catégorie et les mots-clé pour une annonce. Au contraire, nous pouvons facilement trouver les annonces pour une catégorie spécifique ou un mot-clé spécifique par la requête `annonce.find()`
- L'association Commentaire entre Membre et Annonce est modifiée. Nous croyons que les commentaires doivent être les compositions de la relation Annonce, car ils ne sont demandés qu'avec les annonces. En même temps, pour savoir ce qui crée ce commentaire, nous référençons l'objectId du membre dans le commentaire.
- La relation SoldeCompte est intégrée directement dans Membre, car tout le monde n'a qu'un compte, et les virements vont relier les objectId du membre.

Ci-dessous, c'est un UML simple représentant les relations entre eux.

<img src="./mongodb_uml.png" alt="mongodb_uml" style="zoom:80%;" />

[Cliquer ici pour voir le script de créer la base de données de MongoDB](https://gitlab.utc.fr/zhengzil/na17-groupe7/tree/master/POC/mongodb_script)