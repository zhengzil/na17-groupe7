DROP TABLE Membre;
DROP TABLE Annonce;

CREATE OR REPLACE TYPE typAdresse AS OBJECT (
    pays VARCHAR2(50),
    code_postal CHAR(5),
    rue VARCHAR2(255),
    num NUMBER(3),
    appart NUMBER(3));
/
CREATE OR REPLACE TYPE typListeAdresse AS TABLE OF typAdresse;
/
CREATE OR REPLACE TYPE typSoldeCompte AS OBJECT (
    IBAN CHAR(27),
    solde NUMBER(9,2));
/
CREATE OR REPLACE TYPE typMotCles AS TABLE OF VARCHAR2(50);
/
CREATE OR REPLACE TYPE typPhoto AS OBJECT (
    url BLOB,
    hauteur NUMBER(3,2),
    largeur NUMBER(3,2))
/
CREATE OR REPLACE TYPE typListePhoto AS TABLE OF typPhoto;
/
CREATE OR REPLACE TYPE typProduit AS OBJECT (
    nom VARCHAR2(50),
    etat VARCHAR2(20))
/
CREATE OR REPLACE TYPE typListeProduit AS TABLE OF typProduit;
/

CREATE TABLE Membre (
    id NUMBER(5) PRIMARY KEY,
    email VARCHAR2(100) NOT NULL UNIQUE,
    mdp VARCHAR2(255) NOT NULL,
    nom VARCHAR2(50) NOT NULL,
    prenom VARCHAR2(50) NOT NULL,
    tel VARCHAR2(20) UNIQUE,
    semestre CHAR(5),
    adresse typAdresse,
    compte typSoldeCompte,
    adresseLivraison typListeAdresse)
NESTED TABLE adresseLivraison STORE AS membre_nt1;

CREATE TABLE Annonce (
    idann NUMBER(5),
    titre VARCHAR2(255),
    description VARCHAR2(1000) NOT NULL,
    date_publi DATE NOT NULL,
    statut VARCHAR2(10),
    type VARCHAR2(10),
    echange VARCHAR2(255),
    duree INTERVAL DAY TO SECOND,
    prix NUMBER(9,2),
    offert_par NUMBER(5),
    demande_par NUMBER(5),
    motCles typMotCles,
    photos typListePhoto,
    produits typListeProduit,
    PRIMARY KEY (idann),
    FOREIGN KEY (offert_par) REFERENCES Membre(id),
    FOREIGN KEY (demande_par) REFERENCES Membre(id),
    CHECK (statut IN ('en cours', 'terminée', 'annulée')),
    CHECK (type IN ('Troc', 'Emprunt', 'AchatVente')),
    CHECK (NOT (type = 'Troc' AND (prix IS NOT NULL OR duree IS NOT NULL))),
    CHECK (NOT (type = 'Emprunt' AND echange IS NOT NULL)),
    CHECK (NOT (type = 'AchatVente' AND (echange IS NOT NULL OR duree IS NOT NULL))),
    CHECK ((offert_par IS NOT NULL AND demande_par IS NULL) OR (demande_par IS NOT NULL AND offert_par IS NULL)))
NESTED TABLE motCles STORE AS annonce_nt1
NESTED TABLE photos STORE AS annonce_nt2
NESTED TABLE produits STORE AS annonce_nt3;